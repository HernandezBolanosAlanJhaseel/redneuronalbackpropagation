
public class Neurona {
	private int NumEntradas;
	private double error;
	private double entradas[];
	private double peso_entrada[];
	private int idPeso[];
	private double salida;
	private double entradaFinal;
	private double aprendizaje = 0.25;
	private double delta;
	private double sumatoriaDelta;
	private double nuevo_peso_Entrada[];
	public Neurona(int NumEntradas) {
		this.NumEntradas = NumEntradas;
		this.entradas = new double [NumEntradas];
		this.peso_entrada = new double [NumEntradas];
		this.idPeso = new int [NumEntradas];
		this.nuevo_peso_Entrada= new double [NumEntradas];
		entradaFinal=0;
	}
	public int getNumEntradas() {
		return NumEntradas;
	}
	public double getEntradaFinal() {
		return entradaFinal;
	}
		
	public double getSalida() {
		return salida;
	}
	
	public void setSalida(double salida) {
		this.salida = salida;
	}
	
	public double getError() {
		return error;
	}

	public void setError(double error) {
		this.error = error;
	}
	
	public double getDelta() {
		return delta;
	}
	

	public double[] getNuevo_peso_Entrada() {
		return nuevo_peso_Entrada;
	}
	public int[] getIdPeso() {
		return idPeso;
	}

	
	//_----------recibe peso,valorEntrada y el id del peso para futuros cambios---------	
	public void setData(int id, double entrada, double peso, int idpeso) {
		this.entradas[id] = entrada;
		this.peso_entrada[id] = peso;
		this.idPeso[id] = idpeso;
		entradaFinal += (entrada*peso);
	}
	//-------------------------------------------------------------------
	
	
	public void sigmoide() {
		double exp = Math.exp(- (this.entradaFinal));
		double salida = 1/ (1 + exp );
		this.salida =salida;
		System.out.println("this.entrada sigmoide => "+this.entradaFinal+ " aplicando sigmoide => "+ salida);
	}
	
	public double errorNeuronav2(double valorEsperado) {//error real
		double er = (valorEsperado - this.salida);
		this.error = er;
		System.out.println("error => "+ er);
		return er;
	}
	
	public double errorNeurona(double valorEsperado) {
		double potencia =  Math.pow( (valorEsperado - this.salida) , 2);
		double errorFinal = potencia *0.5;
		this.error = errorFinal;

		return errorFinal;
	}
	
	public void CalculaDelta() {
		double delt = this.salida* (1-this.salida)*this.error;
		this.delta =delt;
	}
	
	public void CalcularDeltaOcultas() {
		double delt = this.salida* (1- this.salida) * this.sumatoriaDelta;
		this.delta=delt;
		System.out.println("new delta => "+delt);
	}
	
	public void setDelta(double datos) {
		this.sumatoriaDelta+=datos;
	}
	
	public void generarPesosNuevos () {
		for (int i = 0; i < peso_entrada.length; i++) {
			nuevo_peso_Entrada[i] = (aprendizaje*delta)+peso_entrada[i] ;
			System.out.println("idpeso =>"+ this.idPeso[i] + " antiguo=> "+peso_entrada[i] +" nuevo => "+nuevo_peso_Entrada[i]);
		}
	}
	
	public void calculaErrorSalida() {
		for (int i = 0; i < peso_entrada.length; i++) {
			nuevo_peso_Entrada[i] = (aprendizaje*entradaFinal*delta)+peso_entrada[i] ;
			System.out.println("idpeso =>"+ (this.idPeso[i] +1) + " antiguo=> "+peso_entrada[i] +" nuevo => "+nuevo_peso_Entrada[i]);
		}
	}

}
