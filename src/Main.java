import java.awt.List;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
	private static Scanner r;
	int semilla_random =10000;
	int num_Entradas;
	//guardan los datos de entrada y salida para cada ciclo de entrenamiento
	double entradas[];
	double salidas[];
	 //_____________________________________
	
	 int num_Ocultas;
	 int tam_capas[];
	 int num_Salidas;
	 int num_arregloDePesos;
	 int tam_arregloDePesos[];
	 double matrizPesos[][];
	 double pesos[];
	 int num_neuronasSalidas;
	Neurona data[];
	 double datos_entrenamiento[][]= {
			 {170,56},
			 {172,63},
			 {160,50},
			 {170,63},
			 {174,66},
			 {158,55},
			 {183,80},
			 {182,70},
			 {165,54}
			 };
	 double salida [][]= {
			 {1}, //1,0 == mujer 
			 {0}, // 0 0 == hombre
			 {1},
			 {0},
			 {0},
			 {1},
			 {0},
			 {0},
			 {1}
			 };
	double datos_errorEntrenamiento[][];
	double errorTotalSalidas[];
	public static void main(String[] args) {
			Main m = new Main();
		r = new Scanner (System.in);
		m.inicial();
	}
	
	public void inicial() {
		//capa de entrada
		System.out.println("Numero de entradas ?");
		num_Entradas = r.nextInt();
		entradas = new double[num_Entradas];
		//capas ocultas
		System.out.println("numero de capas ocultas ?");
		num_Ocultas = r.nextInt();
		num_neuronasSalidas=0;
		tam_capas = new int[num_Ocultas];
		for (int i = 0; i < tam_capas.length; i++) {
			System.out.println("tama�o de la capa "+(i+1) +" ?");
			tam_capas[i] = r.nextInt();
			num_neuronasSalidas += tam_capas[i];
		}
		//capa de salida
		System.out.println("numero de salidas ?");
		num_Salidas =  r.nextInt();
		num_neuronasSalidas += num_Salidas;
		salidas = new double[num_Salidas];
		
		//pesos
		num_arregloDePesos = num_Ocultas+1;
		tam_arregloDePesos = new int[num_arregloDePesos];
		System.out.println(tam_arregloDePesos.length);
		int temp=0;
		int num_pesos=0;
		for (int i = 0; i < tam_arregloDePesos.length; i++) {
			if (i == 0) {//entrada * primera oculta
				tam_arregloDePesos[i] = num_Entradas * tam_capas[i];
			}else if(i == (num_Ocultas)) {//ultima oculta * salidas
				tam_arregloDePesos[i] = tam_capas[temp]*num_Salidas;
			}else {//ocultas *ocultas
				tam_arregloDePesos[i] = tam_capas[temp]*tam_capas[temp+1];
				temp++;
			}
			System.out.println("tama�o arreglo pesos => "+ tam_arregloDePesos[i]);
			num_pesos += tam_arregloDePesos[i];
		}
		generar_pesos(num_pesos);
		
		
		//crea matriz de errores
		datos_errorEntrenamiento= new double[datos_entrenamiento.length +1][num_Salidas];
		//errores
		errorTotalSalidas = new double [num_Salidas];
		//asignar entradas
		
		
		asignaEntradas(0);
		generar_Neuronas(0);
		
		for (int i = 0; i < datos_entrenamiento.length; i++) {
			
			while(num_iteracion < limiteIteracion) {
				backpropagation();
				pesosNuevos();
				num_iteracion++;
				generar_Neuronas(i);
			}

			num_iteracion =0;
//			asignaEntradas(i);
//			generar_Neuronas(i);
		}
		//generar error total por salida
	}	
	
	public void generar_pesos(int maximo) {
		pesos= new double [maximo];
		int numero;
		for (int i = 0; i < maximo; i++) {
			//generar pesos random 0-1
			numero = (int) (Math.random() * 100);
			pesos[i]= (double)numero/semilla_random;
			System.out.println("peso "+(i+1) + "=> "+ pesos[i]);
		}
	}
	
	public void asignaEntradas(int posicion) {
		for (int i = 0; i < num_Entradas; i++) {
			entradas[i] = datos_entrenamiento[posicion][i];
		}

		
		for (int i = 0; i < salidas.length; i++) {
			salidas[i] = salida[posicion][i];
			//System.out.println("salida deseada => "+ salidas[i]);
		}
	}
			int num_iteracion  = 0;
			int limiteIteracion = 50;
	public void generar_Neuronas(int posMatrizDatosError) {
		data = new Neurona [num_neuronasSalidas];
		int tamData = num_Entradas;
		int posData = 0;
		int recorrido = tam_capas[0];
		for (int i = 0; i <= num_Ocultas; i++) { 
			for (int j = 0; j < recorrido ; j++) {
				data[posData] = new Neurona(tamData);
//				System.out.println("creada neurona => "+ posData + "de tama�o =>" +tamData);
				posData++;
			}
			tamData = recorrido;
			recorrido = ((i+1) < num_Ocultas) ? tam_capas[i+1] : num_Salidas;
		}
		
		System.out.println("num total salidas => " + num_neuronasSalidas);
		int temp_pesos=0;
		int tam_neuronas_anterior = num_Entradas;
		int posicion_neurona =0;
		int temporal_posicion_neurona=posicion_neurona;
		
		int posNeuronaAlimentadora= 0;
		int temporal_pos_Alimentador=posNeuronaAlimentadora;//temporal posicion de neurona alimentadora
		int tam_capasComparador = 0; 
		// recorre por cada capa oculta
		for (int i = 0; i <= num_Ocultas ;i++) {
			//capa anterior distribuye sus datos a la sig
			
			for (int j = 0; j < tam_neuronas_anterior; j++) {
//				System.out.println("num tam_neorunas anterior => "+tam_neuronas_anterior);
				//sig capa recibe datos de la anterior 
				posicion_neurona = temporal_posicion_neurona;
				tam_capasComparador = (i == num_Ocultas) ? num_Salidas : tam_capas[i]; 
				//genera neuroras necesarias
				for (int k = 0; k < tam_capasComparador; k++) {
	//				System.out.println("tama�o capas =>"+ tam_capasComparador);
					if (i == 0) {//si entrada a capa oculta 1
						System.out.println("neurona => "+ posicion_neurona+ " id => "+j +" entrada=> "+entradas[j]+" peso => "+ pesos[temp_pesos] + " idpeso "+temp_pesos);
						data[posicion_neurona].setData(j, entradas[j], pesos[temp_pesos], temp_pesos);
//						data [posicion_neurona].sumarEntrada(entradas[j] * pesos [temp_pesos] );
//						System.out.println("entrada= >" +entradas[j] +" * peso => "+pesos [temp_pesos]+" igual a=> "+data[posicion_neurona].getEntrada());
					}else {
						System.out.println("neurona => "+ posicion_neurona+ " id => "+j +" entrada=> "+data[posNeuronaAlimentadora].getSalida()+" peso => "+ pesos[temp_pesos] + " idpeso "+temp_pesos);
						data[posicion_neurona].setData(j, data[posNeuronaAlimentadora].getSalida(), pesos[temp_pesos], temp_pesos);
//						data [posicion_neurona].sumarEntrada(data[posNeuronaAlimentadora].getSalida() * pesos [temp_pesos] );
//						System.out.println("entrada= > "+data[posNeuronaAlimentadora].getSalida() +" => "+ pesos[temp_pesos]+" igual a=> "+ data[posicion_neurona].getEntrada());
					}
					temp_pesos++;
					posicion_neurona++;
				}
				if(i != 0) posNeuronaAlimentadora++;
				//fin peso*entrada neorunaEntrada j
			}
			posicion_neurona = temporal_posicion_neurona;
			System.out.println("\n___resultados finales sigmoide____");
			for (int m = 0; m < tam_capasComparador; m++) {
				data [posicion_neurona].sigmoide();
				if (i == num_Ocultas) {
//					System.out.println("para data =>"+ posicion_neurona);
					datos_errorEntrenamiento[posMatrizDatosError][m] = data[posicion_neurona].errorNeuronav2(salida[posMatrizDatosError][m]);
					data[posicion_neurona].CalculaDelta();
					data[posicion_neurona].calculaErrorSalida();					
					//genera datos de error.
					//error_Red_neuronal();
					//modificar pesos => 					
					
				}
				posicion_neurona++;
			}
			//fin capa
			temporal_pos_Alimentador = posNeuronaAlimentadora;
			temporal_posicion_neurona = posicion_neurona;
			tam_neuronas_anterior= tam_capasComparador;
			System.out.println("____________________fin capa_______________________ ");
			//aplica funcion para obtener salida de las neuronas
			
		}
		

		//asignar pesos nuevos= > 
	}
	
	public void backpropagation () {
		System.out.println("______________________________inicia backpropagation _____________________-");
		int posNeuronaSig = num_neuronasSalidas -num_Salidas;
		int tempPosNeuronaSig = posNeuronaSig;
		int neuronaId =posNeuronaSig;
		int tempNeuronaId = neuronaId;
		int tempPosPeso = pesos.length;
		int tamSig = num_Salidas;
		for (int i = (num_Ocultas-1); i>= 0; i--) {
			int posPeso = tempPosPeso - (tam_capas[i] * tamSig);
			tempPosPeso = posPeso;
			neuronaId = tempNeuronaId -tam_capas[i];
			tempNeuronaId = neuronaId;
			for (int j = 0; j < tam_capas[i]; j++) {
				for (int h = 0; h < tamSig; h++) {
					data[neuronaId].setDelta(data[posNeuronaSig].getDelta()*pesos[posPeso]);
					posPeso++;
					posNeuronaSig++;
				} posNeuronaSig = tempPosNeuronaSig;
				data[neuronaId].CalcularDeltaOcultas();
				data[neuronaId].generarPesosNuevos();
				neuronaId++;
			} tamSig = tam_capas[i];
			posNeuronaSig = tempPosNeuronaSig- tam_capas[i];
			tempPosNeuronaSig = posNeuronaSig;
		}
		
	}
	public void error_Red_neuronal() {
		//calcular error total de cada salida por todos los entrenamientos
		for (int i = 0; i < datos_errorEntrenamiento.length; i++) {
			//System.out.print("errores por salida => ");
			for (int j = 0; j < num_Salidas; j++) {
				if (i == (datos_errorEntrenamiento.length -1)) {	
					errorTotalSalidas[j] =  datos_errorEntrenamiento[i][j];
					System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1");
					System.out.println("error total salida "+(j+1)+" => "+ errorTotalSalidas[j]);
				}else datos_errorEntrenamiento[datos_errorEntrenamiento.length -1][j] += datos_errorEntrenamiento[i][j];
				// System.out.print(datos_errorEntrenamiento[i][j] +"  ,  ");
			} // System.out.println("");
		} // System.out.println();
	}
	
	public void pesosNuevos() {
		for (int i = 0; i < num_neuronasSalidas; i++) {
			int []ids = data[i].getIdPeso();
			double []nuevosPesos =data[i].getNuevo_peso_Entrada();
			for (int j = 0; j < nuevosPesos.length; j++) {
				pesos[ids[j]] = nuevosPesos[j];
				System.out.println("peso id => "+ids[j] +"ahora vale => "+ nuevosPesos[j]);
			}
		}
	}
}
